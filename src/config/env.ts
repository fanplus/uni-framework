const apiEnv: ApiEnv = 'dev'

const envMap = {
  dev: {
    baseUrl: 'https://www.taotikee.com/',
  },
  beta: {
    baseUrl: 'http://m.beta.xxx.com',
  },
  prod: {
    baseUrl: 'https://m.xxx.com',
  },
  local: {
    baseUrl: 'http://m.dev.xxx.com',
  },
}

type ApiEnv = keyof typeof envMap
type Env<T extends ApiEnv> = {
  apiEnv: T
} & typeof envMap[T]

function createEnv(apiEnv: ApiEnv): Env<typeof apiEnv> {
  return Object.assign({ apiEnv }, envMap[apiEnv])
}

const env = createEnv(apiEnv)
export default env
