export const useTitle = () => {
  let oldValue = 'Hello'
  let newValue = 'Word'
  const title = ref(oldValue)
  function changeTitle(val: any) {
    // oldValue = title.value
    // title.value = newValue
    // newValue = oldValue
    title.value = val;
  }
  return {
    title,
    changeTitle,
  }
}
