
import {
	isWeixinClient,
	currentPage,
	trottle,
} from './tools'

export const toLogin = trottle(_toLogin, 1000)
// 去登录
function _toLogin() {
	//#ifdef APP-PLUS || MP-WEIXIN || MP-TOUTIAO
	uni.navigateTo({
		url: '/pages/login/login'
	});
	//#endif
	//#ifdef  H5
	const pathRecharge = 'pages/login/login'
	let path = currentPage().route
	if (path != pathRecharge) {
		uni.navigateTo({
			url: '/pages/login/login'
		})
	}
	// #endif
}