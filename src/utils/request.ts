// 引入 uni-ajax 模块
import ajax from 'uni-ajax'
// 引入环境
import env from '@/config/env';

// 创建请求实例
const instance = ajax.create({
  // 初始配置
  baseURL: env.baseUrl
})

// 添加请求拦截器
instance.interceptors.request.use(
  config => {
    // 在发送请求前做些什么
    // 添加 token
    const token = uni.getStorageSync('TOKEN') || 123;
    token && (config.header['Authorization'] = token)
    return config
  },
  error => {
    // 对请求错误做些什么
    uni.showToast({
      title: '请求出错',
      duration: 2000,
      icon: "none",
    });
    return Promise.reject(error)
  }
)

// 添加响应拦截器
instance.interceptors.response.use(
  response => {
    console.log('response', response);
    let msg = '';
    // 对响应数据做些什么
    if (response.data?.code === 401) {
      uni.removeStorageSync('token')
      // alert('即将跳转登录页。。。', '登录过期')
      // setTimeout(redirectHome, 1500)
      uni.navigateTo({
        url: "/pages/loign/login",
      });
      return;
    }
    if (response.data?.code === 500) {
      msg = '服务器内部错误';
      return;
    }
    if (response.data?.code === 200) {
      return response.data;
    }
    
  },
  error => {
    // 对响应错误做些什么
    console.log('error', error);
    uni.showToast({
      title: '请求响应出错',
      duration: 2000,
      icon: "none",
    });
    return Promise.reject(error)
  }
)

// 导出 create 创建后的实例
export default instance