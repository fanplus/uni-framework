
import defHttp from '@/utils/request' // 路径需根据项目实际情况


enum Api {
  fetchList = '/hospital/page',
  queryPage = '/bus/eval/query/page',
  addObj = '/bus/eval/add',
}


export function fetchList(params?: object) {
  return defHttp.post({ url: Api.fetchList, data: params });
}
