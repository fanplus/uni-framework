export default defineStore({
  id: 'user',
	persist: {
	  // 开启持久化
	  enabled: true,
	  H5Storage: window?.localStorage,
	  strategies: [
	    {
	      key: 'user',
	      storage: window?.localStorage,
	    },
	  ],
	},
  state: () => {
    return {
      userInfo: null,
			token: undefined,
    }
  },
  getters: {
		getUserInfo(state: any) {
			console.log('state', state);
			return state.userInfo;
		},
		getToken(state: any): string {
			return state.token || '';
		},
    logged: (state: any) => {
      return !!state.token
    },
  },
  actions: {
    setUserInfo(userInfo: any) {
      Object.assign(this.userInfo, userInfo)
    },
    async login(account, pwd) {
      // const { data } = await api.login({ account: 'xxx', pwd: 'xxx' })
      // this.setUserInfo(data.userInfo) // 调用另一个 action 的方法
      // const appStore = useAppStore()
      // appStore.setData(data) // 调用 app store 里的 action 方法
      // return data
    },
  },
})
