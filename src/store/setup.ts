// import { defineStore } from 'pinia';
// import { ref, computed } from 'vue';

import { pinia } from '@/modules/pinia';

export const useSetupStore =  defineStore(
  'setup',
  () => {
    const name = ref<string>('allen')
    const token = ref<string>('token...')

    const fullName = computed(() => {
      return name.value + ' ttk'
    })

    const updateName = (val: any) => {
      console.log('val', val);
      name.value = val
    }
    return {
      name,
      token,
      fullName,
      updateName,
    }
  },
  {
    persist: {
      // 开启持久化
      enabled: true,
    },
  }
)

// Need to be used outside the setup
export function useSetupStoreWidthOut() {
  return useSetupStore(pinia)
}
