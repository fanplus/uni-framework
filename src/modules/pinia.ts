
import type { App } from 'vue'
import { createPinia } from 'pinia'
import piniaPluginPersist from 'pinia-plugin-persist-uni';

export const pinia = createPinia().use(piniaPluginPersist)

export const setupPinia = (app: App<Element>) => app.use(pinia)