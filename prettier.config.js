module.exports = {
  printWidth: 100, // 一行的字符数，如果超过会进行换行，默认为100
  tabWidth: 2, // 一个 tab 代表几个空格数，默认为 2 个
  semi: true, // 行尾是否使用分号，默认为true
  vueIndentScriptAndStyle: true,
  singleQuote: true, // 字符串是否使用单引号，默认为 false，使用双引号
  trailingComma: 'none', // 是否使用尾逗号
  proseWrap: 'never',
  htmlWhitespaceSensitivity: 'strict',
  endOfLine: 'auto',
};
