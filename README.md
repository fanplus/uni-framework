


### unplugin-vue-components
使用此插件可以自动导入组件,但是 uni 有 easycom 可以自动扫描组件, 如果使用第三方UI框架，可以配置下自动导入

使用 easycom 时有些组件无法统一导入,需要收到导入
```javascript
import HelloWord from '@/components/hello/TimetableContent.vue'

```
### unplugin-auto-import/vite
使用此插件可以自动导入 ref 等相关内容


```javascript
// 引入前
import { ref, computed } from 'vue'
const count = ref(0)
const doubled = computed(() => count.value * 2)

//引入后
const count = ref(0)
const doubled = computed(() => count.value * 2)

```
